# Task Manager Backend API (NodeJS, Express, Mongoose, Swagger)

# Install dependencies

npm install

# Run server

npm start

# Debug mode

DEBUG=tasks-api:* npm start

# Access Swagger documentation from http://localhost:3000/api-docs